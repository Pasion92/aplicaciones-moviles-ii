package ViewModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetalleUsuario {

    @SerializedName("estado")
    @Expose
    private Boolean estado;
    @SerializedName("usuario")
    @Expose
    private String usuario;
    @SerializedName("password")
    @Expose
    private String password;

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public DetalleUsuario withEstado(Boolean estado) {
        this.estado = estado;
        return this;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public DetalleUsuario withUsuario(String usuario) {
        this.usuario = usuario;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public DetalleUsuario withPassword(String password) {
        this.password = password;
        return this;
    }

}
