package ViewModels;

public class Registro_Usuario {
    public String estado;
    public String username;
    public String password;
    public String detalle;

    public Registro_Usuario() {
    }

    public Registro_Usuario(String estado, String username, String password, String detalle) {
        this.estado = estado;
        this.username = username;
        this.password = password;
        this.detalle = detalle;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }
}
