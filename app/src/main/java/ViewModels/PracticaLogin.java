package ViewModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PracticaLogin {

    @SerializedName("estado")
    @Expose
    private Boolean estado;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("usuario")
    @Expose
    private String usuario;

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public PracticaLogin withEstado(Boolean estado) {
        this.estado = estado;
        return this;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public PracticaLogin withToken(String token) {
        this.token = token;
        return this;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public PracticaLogin withUsuario(String usuario) {
        this.usuario = usuario;
        return this;
    }

}
