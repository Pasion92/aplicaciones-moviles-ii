package ViewModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UsuarioGet {

    @SerializedName("estado")
    @Expose
    private Boolean estado;
    @SerializedName("usuarios")
    @Expose
    private List<Usuario> usuarios = null;

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public UsuarioGet withEstado(Boolean estado) {
        this.estado = estado;
        return this;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public UsuarioGet withUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
        return this;
    }

}
