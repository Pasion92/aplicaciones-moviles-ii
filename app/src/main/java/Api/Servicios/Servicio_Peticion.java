package Api.Servicios;

import ViewModels.DetalleUsuario;
import ViewModels.Login;
import ViewModels.NumerosAleatorios;
import ViewModels.Peticion_Noticia;
import ViewModels.PracticaLogin;
import ViewModels.RegistroNotificacion;
import ViewModels.Registro_Usuario;
import ViewModels.UsuarioGet;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Servicio_Peticion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario> registrarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/numAlea")
    Call<NumerosAleatorios> numerosAleatorios();

    @FormUrlEncoded
    @POST("api/login")
    Call<Login> getLogin(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/loginSocial")
    Call<PracticaLogin> getLoginPractica(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/detallesUsuario")
    Call<DetalleUsuario> getDetalles(@Field("usuarioId") int id);

    @GET("api/todasNot")
    Call<Peticion_Noticia> getNoticias();

    @POST("api/todosUsuarios")
    Call<UsuarioGet> Users();

    @FormUrlEncoded
    @POST("api/crearNotUsuario")
    Call<RegistroNotificacion> registrarNota(@Field("usuarioId") String correo, @Field("titulo") String contrasenia, @Field("descripcion") String desc);
}
