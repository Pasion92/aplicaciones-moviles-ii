package com.example.plataformaupt;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import Api.Api;
import Api.Servicios.Servicio_Peticion;
import ViewModels.DetalleUsuario;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PantallaDetalle extends AppCompatActivity {
    TextView id, pass;
    String parametro, cod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_detalle);
        id = (TextView) findViewById(R.id.id);
        pass = (TextView) findViewById(R.id.pass);
        parametro = getIntent().getExtras().getString("parametro");
        cod = getIntent().getExtras().getString("cod");


        Servicio_Peticion service = Api.getApi(PantallaDetalle.this).create(Servicio_Peticion.class);
        Call<DetalleUsuario> loginCall = service.getDetalles(Integer.parseInt(cod));
        loginCall.enqueue(new Callback<DetalleUsuario>() {
            @Override
            public void onResponse(Call<DetalleUsuario> call, Response<DetalleUsuario> response) {
                DetalleUsuario peticion = response.body();
                if (peticion.getEstado()) {
                    id.setText(parametro+ "  id:"+cod);
                    pass.setText(peticion.getPassword());
                } else {
                    Toast.makeText(PantallaDetalle.this, "Datos Incorrectos", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<DetalleUsuario> call, Throwable t) {
                Toast.makeText(PantallaDetalle.this, "Error :(", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
