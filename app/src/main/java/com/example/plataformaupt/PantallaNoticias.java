package com.example.plataformaupt;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import Api.Api;
import Api.Servicios.Servicio_Peticion;
import ViewModels.Detalle;
import ViewModels.Peticion_Noticia;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PantallaNoticias extends AppCompatActivity {
    Button obtener;
    EditText panel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_noticias);
        obtener = (Button) findViewById(R.id.obtener);
        panel = (EditText) findViewById(R.id.panel);
        List<Peticion_Noticia> nota = new ArrayList<>();
        List<Detalle> family = new ArrayList<>();
        obtener.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Servicio_Peticion service = Api.getApi(PantallaNoticias.this).create(Servicio_Peticion.class);
                Call<Peticion_Noticia> noticias =  service.getNoticias();
                noticias.enqueue(new Callback<Peticion_Noticia>() {
                    @Override
                    public void onResponse(Call<Peticion_Noticia> call, Response<Peticion_Noticia> response) {
                        Peticion_Noticia peticion = response.body();
                        if(peticion.estado == "true"){
                            panel.setText(String.valueOf(peticion.detalle.get(0).descripcion));
                            Toast.makeText(PantallaNoticias.this, "Petición siendo procesada", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(PantallaNoticias.this, "Petición no procesada", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_Noticia> call, Throwable t) {
                        t.printStackTrace();
                        panel.setText(t.getMessage());
                    }
                });
            }
        });
    }
}
