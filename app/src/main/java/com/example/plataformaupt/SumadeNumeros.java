package com.example.plataformaupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import Api.Api;
import Api.Servicios.Servicio_Peticion;
import ViewModels.NumerosAleatorios;
import ViewModels.Registro_Usuario;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SumadeNumeros extends AppCompatActivity {
    public Button obtener;
    public TextView uno, dos, sum, ganar, perder;
    public int contador = 0, perdiste=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sumade_numeros);
        obtener = (Button) findViewById(R.id.obtener);
        uno = (TextView) findViewById(R.id.numero1);
        dos = (TextView) findViewById(R.id.numero2);
        sum = (TextView) findViewById(R.id.sum);
        ganar = (TextView) findViewById(R.id.ganar);
        perder = (TextView) findViewById(R.id.perder);
        obtener.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Api api = new Api();
                Servicio_Peticion service = api.getApi(SumadeNumeros.this).create(Servicio_Peticion.class);
                Call<NumerosAleatorios> registrarCall = service.numerosAleatorios();
                registrarCall.enqueue(new Callback<NumerosAleatorios>() {
                    @Override
                    public void onResponse(Call<NumerosAleatorios> call, Response<NumerosAleatorios> response) {
                        NumerosAleatorios peticion = response.body();
                        uno.setText(String.valueOf(peticion.n1));
                        dos.setText(String.valueOf(peticion.n2));
                        int suma = peticion.n1 + peticion.n2;
                        sum.setText("Suma: "+suma);
                        if (suma == 15) {
                            Toast.makeText(SumadeNumeros.this, "Ganaste", Toast.LENGTH_LONG).show();
                            contador++;
                            ganar.setText("Ganaste: "+contador);
                            if (contador >= 5){
                                Intent intent = new Intent(SumadeNumeros.this, MainActivity.class);
                                startActivity(intent);
                            }
                        } else {
                            Toast.makeText(SumadeNumeros.this, "Perdiste", Toast.LENGTH_LONG).show();
                            perdiste++;
                            perder.setText("Perdiste: "+perdiste);
                        }
                        if (response.body() == null) {
                            Toast.makeText(SumadeNumeros.this, "Ocurrio un Error, intentalo más tarde", Toast.LENGTH_LONG).show();
                            return;
                        }
                    }

                    @Override
                    public void onFailure(Call<NumerosAleatorios> call, Throwable t) {
                        Toast.makeText(SumadeNumeros.this, "Erro :(", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
