package com.example.plataformaupt;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ViewModels.Usuario;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyviewHolder> {

    Context context;
    List<Usuario> usuariosList;
    int globalposition;
    String parametro;

    public RecyclerAdapter(Context context, List<Usuario> usuariosList) {
        this.context = context;
        this.usuariosList = usuariosList;
    }

    public void SetUsuarios(List<Usuario> usuariosList) {
        this.usuariosList = usuariosList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerAdapter.MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_layout, parent, false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter.MyviewHolder holder, int position) {
        holder.tvMovieName.setText("Nombre: " + usuariosList.get(position).getUsername());
        holder.cod.setText(usuariosList.get(position).getId().toString());
        holder.setOnClickListeners();
    }

    @Override
    public int getItemCount() {
        if (usuariosList != null) {
            return usuariosList.size();
        }
        return 0;

    }

    public class MyviewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvMovieName, cod;
        ImageView image;
        public MyviewHolder(final View itemView) {
            super(itemView);
            context = itemView.getContext();
            tvMovieName = (TextView) itemView.findViewById(R.id.textViewMovieName);
            cod = (TextView) itemView.findViewById(R.id.cod);
        }

        void setOnClickListeners(){
            tvMovieName.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, PantallaDetalle.class);
            // envías los TextView de esta forma:
            intent.putExtra("parametro", tvMovieName.getText());
            intent.putExtra("cod", cod.getText());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }
}