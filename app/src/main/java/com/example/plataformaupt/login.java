package com.example.plataformaupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import Api.Api;
import Api.Servicios.Servicio_Peticion;
import ViewModels.PracticaLogin;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class login extends AppCompatActivity {
    EditText correo, contrasena;
    Button login;
    public String APITOKEN = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if (token != "") {
            Toast.makeText(login.this, "Bienvenido Nuevamente", Toast.LENGTH_LONG).show();
            startActivity(new Intent(login.this, PantallaUsuarios.class));
        }

        correo = (EditText) findViewById(R.id.correo);
        contrasena = (EditText) findViewById(R.id.contrasena);
        login = (Button) findViewById(R.id.login);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!correo.getText().toString().isEmpty() && !contrasena.getText().toString().isEmpty()) {
                    Servicio_Peticion service = Api.getApi(login.this).create(Servicio_Peticion.class);
                    Call<PracticaLogin> loginCall = service.getLoginPractica(correo.getText().toString(), contrasena.getText().toString());
                    loginCall.enqueue(new Callback<PracticaLogin>() {
                        @Override
                        public void onResponse(Call<PracticaLogin> call, Response<PracticaLogin> response) {
                            PracticaLogin peticion = response.body();
                            if (peticion.getEstado()) {
                                APITOKEN = peticion.getToken();
                                guardarPreferencias();
                                Toast.makeText(login.this, "Bienvenido", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(login.this, PantallaUsuarios.class));
                            } else {
                                Toast.makeText(login.this, "Datos Incorrectos", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<PracticaLogin> call, Throwable t) {
                            Toast.makeText(login.this, "Error :(", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    Toast.makeText(login.this, "Llene campos please", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void guardarPreferencias() {
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = APITOKEN;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("TOKEN", token);
        editor.commit();
    }
}
