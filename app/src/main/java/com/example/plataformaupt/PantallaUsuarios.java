package com.example.plataformaupt;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import Api.Api;
import Api.Servicios.Servicio_Peticion;
import ViewModels.Usuario;
import ViewModels.UsuarioGet;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PantallaUsuarios extends AppCompatActivity {
    List<Usuario> usuariosList;
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_usuarios);

        usuariosList = new ArrayList<>();
        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerAdapter = new RecyclerAdapter(getApplicationContext(),usuariosList);
        recyclerView.setAdapter(recyclerAdapter);

        Servicio_Peticion service = Api.getApi(PantallaUsuarios.this).create(Servicio_Peticion.class);
        Call<UsuarioGet> noticias =  service.Users();
        noticias.enqueue(new Callback<UsuarioGet>() {
            @Override
            public void onResponse(Call<UsuarioGet> call, Response<UsuarioGet> response) {

                UsuarioGet user= response.body();

                if (user.getEstado()){
                    recyclerAdapter.SetUsuarios(user.getUsuarios());
                } else
                    Toast.makeText(PantallaUsuarios.this, "Petición no procesada", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<UsuarioGet> call, Throwable t) {
                Log.d("TAG","Response = "+t.toString());
                Toast.makeText(PantallaUsuarios.this, "Error: " + t.getMessage(), Toast.LENGTH_LONG).show();
                t.printStackTrace();
            }
        });

    }
}
